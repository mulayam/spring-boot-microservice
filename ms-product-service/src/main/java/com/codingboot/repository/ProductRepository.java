package com.codingboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.codingboot.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

}
