package com.codingboot.command;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.codingboot.vo.ProductOderSummary;

@Component
public class ProductOrderCommand {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private Environment env;

	public List<ProductOderSummary> invokeProductOderSummary() {
		ProductOderSummary[] list = restTemplate
				.getForObject(env.getProperty("endpoint.ms-order-service.order-summary"), ProductOderSummary[].class);

		return Arrays.asList(list);

	}
}
