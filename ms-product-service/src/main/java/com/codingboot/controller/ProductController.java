package com.codingboot.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codingboot.command.ProductOrderCommand;
import com.codingboot.config.PropConfig;
import com.codingboot.entity.Product;
import com.codingboot.repository.ProductRepository;
import com.codingboot.vo.ProductOderSummary;

@RestController
public class ProductController {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProductOrderCommand productOrderCommand;

	@Autowired
	private Environment env;
	@Autowired
	private PropConfig propConfig;

	@GetMapping("/product-list")
	public ResponseEntity<List<Product>> getProductList() {

		return ResponseEntity.ok(productRepository.findAll());
	}

	@GetMapping("/product-order-summary")
	public ResponseEntity<List<ProductOderSummary>> getProductOrderSummary() {

		List<Product> productList = productRepository.findAll();

		List<ProductOderSummary> orderSummary = productOrderCommand.invokeProductOderSummary();

		List<ProductOderSummary> list = orderSummary.stream().map(pos -> {
			Product p = productList.stream().filter(e -> e.getId() == pos.getPid()).findFirst().get();
			pos.setName(p.getName());
			return pos;
		}).collect(Collectors.toList());

		return ResponseEntity.ok(list);
	}

	@GetMapping("/customers")
	public String getCustomerList() {
		return propConfig.getCustomerListEndpoint();
	}

}
