package com.codingboot.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
@RefreshScope
public class PropConfig {

	
	@Autowired
	private Environment env;

	public String getCustomerListEndpoint() {
		return env.getProperty("endpoint.customer.customers");
	}
}
