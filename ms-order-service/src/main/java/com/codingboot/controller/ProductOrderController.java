package com.codingboot.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codingboot.vo.ProductOrderSummary;

@RestController
public class ProductOrderController {

	@GetMapping("/order-summary")
	public ResponseEntity<List<ProductOrderSummary>> getProductOrderSummary() {

		List<ProductOrderSummary> list = Arrays.asList(

				new ProductOrderSummary(1, 1232), new ProductOrderSummary(2, 5454), new ProductOrderSummary(3, 6323),
				new ProductOrderSummary(4, 655)

		);
		return ResponseEntity.ok(list);

	}
}
